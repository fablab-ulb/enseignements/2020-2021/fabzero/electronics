/*
* Small piece of plastic to be glued under "milled USB connector"
* Author : Nicolas De Coster
* Date : 2021-03-12
*/
w=11.5;
l=15;
h=0.9;
e=0.1;
alpha=35;
difference(){
    cube([w, l, h]);
    translate([-e,0,0])rotate([alpha,0,0])cube([w+2*e, l, h]);
}