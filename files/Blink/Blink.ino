/*
// Blink.ino
//
// Will blink the LED at INTERVAL interval.
//
// Nicolas De Coster 2021-03-012
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
// 
*/

#define LED 15
#define INTERVAL 1000

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED as an output.
  pinMode(LED, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
      //invert LED output
      digitalWrite(LED, !digitalRead(LED));
      //wait a half period
      delay(INTERVAL/2);
}
