
# Entrées

## Interrupteurs

### MODULE INTERRUPTEUR FIN DE COURSE

* Fonctionne aussi en 3V3
* Connexion : entrée logique

[VMA327](https://www.velleman.eu/products/view/?id=439190)

### CAPTEUR DE VIBRATIONS / CHOCS

* Fonctionne aussi en 3V3
* Connexion : entrée logique

[VMA312](https://www.velleman.eu/products/view/?id=435538)

### MODULE INTERRUPTEUR REED MAGNÉTIQUE

* Fonctionne aussi en 3V3
* Connexion : entrée logique

[VMA308](https://www.velleman.eu/products/view/?id=435530)

### CODEUR ROTATIF NUMÉRIQUE

[VMA435](https://www.velleman.eu/products/view/?id=439226)

## Capteurs analogiques

### CAPTEUR DE TÉMPÉRATEUR ANALOGIQUE

* Fonctionne aussi en 3V3
* Connexion : entrée analogique

[VMA320](https://www.velleman.eu/products/view/?id=435554)

### CAPTEUR CAPACITIF

* Amplificateur intégré au module
* Connexion : entrée analogique

[VMA305](https://www.velleman.eu/products/view/?id=435524)

### CAPTEUR DE COURANT ACS712

* Courant max : 20A
* Connexion : entrée analogique
* Doit être alimenté en 5V

[VMA323](https://www.velleman.eu/products/view/?id=439182)

### CAPTEUR DE FRÉQUENCE CARDIAQUE

* Optique (LED)
* Connexion : entrée analogique

[VMA340](https://www.velleman.eu/products/view/?id=450580)

## Détecteurs

### DÉTECTEUR D'HUMIDITÉ DU SOL & DÉTECTEUR DE NIVEAU D'EAU

* Amplificateur intégré au module
* Fonctionne aussi en 3V3

[VMA303](https://www.velleman.eu/products/view/?id=435520)

### MODULE DE DÉTECTION SONORE AVEC MICROPHONE

* Amplificateur intégré au module
* Connexion : entrée analogique ou logique

[VMA309](https://www.velleman.eu/products/view/?id=435520)

### CAPTEUR DE MOUVEMENT

* Doit être alimenté en 5V
* Connexion : entrée logique

[VMA314](https://www.velleman.eu/products/view/?id=435542)

### CAPTEUR À EFFET HALL

* Doit être alimenté en 5V
* Connexion : entrée logique

[VMA313](https://www.velleman.eu/products/view/?id=435540)

### RÉCEPTEUR 1838 INFRAROUGE 37.9kHz

[VMA317](https://www.velleman.eu/products/view/?id=435548)

## Capteurs numériques

### CAPTEUR DE COULEUR TCS3200

[VMA325](https://www.velleman.eu/products/view/?id=439186)

### CAPTEUR DE TEMPÉRATURE DS18b20

[VMA324](https://www.velleman.eu/products/view/?id=439184)

### ACCÉLÉROMÈTRE NUMÉRIQUE 3 AXES MMA8452

* Doit être alimenté en 3V3
* Connexion : port I2C

[VMA208](https://www.velleman.eu/products/view/?id=439582)

### CAPTEUR DE TEMPS DE VOL VL53L0X

* Optique (LASER)
* Connexion : port I2C

[VMA337](https://www.velleman.eu/products/view/?id=450562)

# Sorties

## Optiques

### MODULE DE TRANSMISSION INFRAROUGE

* Fonctionne entre 1V2 et 1V6
* Nécéssite une résistance de protection
* En 5V, mettre une résistance de 220ohm en série avec la LED

[VMA316](https://www.velleman.eu/products/view/?id=435546)

### MODULE LED RGB

* Nécéssite une résistance de protection
* En 5V, mettre une résistance de 220ohm en série avec chaque LED

[VMA318](https://www.velleman.eu/products/view/?id=435550)

### AFFICHEUR 4 DIGITS AVEC DRIVER TM1637

[VMA425](https://www.velleman.eu/products/view/?id=439208)

### ÉCRAN OLED 0.96"

* Graphique
* Connexion : port I2C

[VMA438](https://www.velleman.eu/products/view/?id=439232)

## Électro-mécanique / Audio

### MODULE RELAIS 5V

* Doit être alimenté en 5V
* Ne pas utiliser en PWM !

[VMA406](https://www.velleman.eu/products/view/?id=435570)

### ÉLECTRO-AIMANT

* Consomme 400mA en 5V, prévoir alimentation séparée
* Fonctionne aussi en 3V3, avec alimentation séparée
* Ne pas utiliser en PWM !

[VMA431](https://www.velleman.eu/products/view/?id=439220)

### MODULE BUZZER

[VMA319](https://www.velleman.eu/products/view/?id=435552)

## Interface de puissance

### MODULE DE PILOTAGE MOS

[VMA411](https://www.velleman.eu/products/view/?id=435580)

### MODULE CONTRÔLEUR DE MOTEUR L298N

* Peut contrôler deux moteurs DC ou un moteur à pas
* Compatible 5V et 3V3, avec alimentation moteur séparée
* Connexion : 4 sorties logiques (1/pôle moteur)

[VMA409](https://www.velleman.eu/products/view/?id=435576)

# Autres

### MODULE D'ALIMENTATION 2 CANAUX 3.3V / 5V

* Courant max. par canal: 500mA
* Connexion : GND ou rien

[VMA424](https://www.velleman.eu/products/view/?id=439206)
